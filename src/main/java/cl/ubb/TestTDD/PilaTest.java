package cl.ubb.TestTDD;

public class PilaTest {
	int t=2, c=0;
	
	public boolean PilaVacia(int i[]) {
	    for (int j=0; j<t; j++)
	    {
	      if(i[j]!=0){
	    	  return false;
	      }
	    }
		return true;
	}
	
	public int PilaStack(int i[]) {
		return t;
	}
	
	public int PilaPop(int i[]) {
		for(int j=t-1; j>=0; j--){
			if(i[j]!=0){
				c=i[j];
				i[j]=0;
				j=-1;}	
		}
		t--;
		return c;
	}
	
	public int PilaTop(int i[]) {
		for(int j=t-1; j>=0; j--){
			if(i[j]!=0){
				c=i[j];
				j=-1;}	
		}
		return c;
	}
}
